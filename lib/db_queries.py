from pymongo import MongoClient
import re
import logging
from env import *

url = DB_URL
name = DB_NAME

def get_connection():
    client = MongoClient(url)
    return client

def end_connection(client):
    client.close()
    return 1

def set_slot(sender_id,slot_name,slot_value):
    try:
        client = get_connection()
        db = client[name]
        coll = db.slots
        coll.update({"sender_id":sender_id},{"$set":{slot_name:slot_value}},True)
        return 1
    except BaseException as e:
        return None
    finally:
        end_connection(client)


def set_multiple_slots(sender_id,slots):
    try:
        client = get_connection()
        db = client[name]
        coll = db.slots
        coll.update({"sender_id":sender_id},{"$set":slots},True)
        return None
    except BaseException as e:
        return None
    finally:
        end_connection(client)

def get_slot(sender_id,slot_name):
    try:
        client = get_connection()
        db = client[name]
        coll = db.slots
        slot_value = None
        result = coll.find_one({"sender_id":sender_id})
        if result is not None and slot_name in result and result[slot_name] is not None:
            slot_value = result[slot_name]
        else:
            return None
        return slot_value
    except BaseException as e:
        return None
    finally:
        end_connection(client)

def get_multiple_slot(sender_id):
    try:
        client = get_connection()
        db = client[name]
        coll = db.slots
        slot_value = None
        result = coll.find_one({"sender_id":sender_id})
        print(result)
        if result is not None:
            return result
        else:
            return None
    except BaseException as e:
        return None
    finally:
        end_connection(client)